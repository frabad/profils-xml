# Fiche de profil personnel en XML

`profils.dtd`
:   modèle de la fiche de profil

`profil.py`
:   script de rédaction assistée de profil.
    Nécessite le module [garden](https://gitlab.com/frabad/garden)
    ou son sous-dossier `garden`.

`profil_tr.py`
:   script de transformation automatisée de profil.
    Nécessite les modules [garden](https://gitlab.com/frabad/garden)
    et [lxml](https://pypi.org/project/lxml/).

`exemples/*`
:   fichiers de démonstration

`xslt/*.xsl`
:   feuilles de transformation d'une instance XML vers d'autres formats
    de sortie

`xslt/profils.css`
:   styles CSS de la sortie HTML

