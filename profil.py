#!/usr/bin/env python3

try:
    import garden
    import garden.text
    import garden.ui
    import pathlib
except ImportError as e:
    print(e,
        "\nInstallez le module 'garden' ou copiez son sous-dossier 'garden'.",
        "\n<https://gitlab.com/frabad/garden/>")

if __name__ == "__main__":
    """
    set the pseudo-model of a tree
    """
    seed = (
        "profil",
            ("prénom",),
            ("nom",),
            ("qualités",),
            ("description",),
    )
    root = garden.tree.fromtuples(seed)
    """
    modify the leaves of a tree
    """
    root = garden.ui.FieldSet(root).xml
    """
    set an ID value and output filename based on the first two children values
    """
    xmlid = garden.text.idize([root[i].text for i in range(2)])
    xmlof = pathlib.Path(xmlid+".xml")
    """
    set attributes
    """
    root.attrib.update({
        "xml:id": xmlid,
        "image": xmlid+".jpg",
        "xml:lang": "any"
    })
    """
    make a tree, grow it, prettify it, write it, check it
    """
    tree = garden.ElementTree(root)
    tree.grow()
    tree.indent()
    tree.topath(xmlof, encoding="UTF-8", xml_declaration="yes")
    xml, error = garden.tree.frompath(xmlof)
    if xml:
        print("Le document '%s' est syntaxiquement correct." % xmlof)
    if error:
        print("Le document '%s' contient une erreur:\n\t%s" % (xmlof, error))
    
