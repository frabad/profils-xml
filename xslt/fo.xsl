<?xml version="1.0" encoding="utf-8"?>

<xsl:transform version="1.0"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink">
  
  <xsl:import href="styles.xsl"/>
  
  <xsl:template match="profils">
    <fo:root>

      <fo:layout-master-set>
        <fo:simple-page-master master-name="page" xsl:use-attribute-sets="sty.page">
          <fo:region-body xsl:use-attribute-sets="sty.page.body"> </fo:region-body>
          <fo:region-before xsl:use-attribute-sets="sty.page.header-height"> </fo:region-before>
          <fo:region-after xsl:use-attribute-sets="sty.page.footer-height"> </fo:region-after>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:declarations>
      </fo:declarations>

      <fo:page-sequence master-reference="page" xsl:use-attribute-sets="sty.default">
        <fo:title>catalogue de profils</fo:title>

        <fo:static-content flow-name="xsl-region-before">
          <fo:block xsl:use-attribute-sets="sty.page.header"> </fo:block>
        </fo:static-content>

        <fo:static-content flow-name="xsl-region-after">
        </fo:static-content>

        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates select="." mode="table" />
          
          <fo:block id="lastpage">
            <xsl:comment>empty block for last-page reference</xsl:comment>
          </fo:block>
          
        </fo:flow>

      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template mode="table" match="*">
    <fo:table table-layout="fixed" border-collapse="separate" width="100%">
      <xsl:apply-templates mode="table.column" select="profil[1]" />
      <fo:table-body>
        <xsl:apply-templates mode="table.row" select="*" />
      </fo:table-body>
    </fo:table>
  </xsl:template>
  
  <xsl:template mode="table.column" match="profil">
    <xsl:apply-templates mode="table.column" select="@image|*"/>
  </xsl:template>
  
  <xsl:template mode="table.column" match="*">
    <fo:table-column>
      <xsl:attribute name="column-width">
        <xsl:text>proportional-column-width(1)</xsl:text>
      </xsl:attribute>
    </fo:table-column>
  </xsl:template>
  
  <xsl:template mode="table.column" match="@image">
    <fo:table-column>
      <xsl:attribute name="column-width">
        <xsl:text>proportional-column-width(2)</xsl:text>
      </xsl:attribute>
    </fo:table-column>
  </xsl:template>
  
  <xsl:template mode="table.column" match="description">
    <fo:table-column>
      <xsl:attribute name="column-width">
        <xsl:text>proportional-column-width(3)</xsl:text>
      </xsl:attribute>
    </fo:table-column>
  </xsl:template>
  
  <xsl:template mode="table.column" match="qualités">
    <fo:table-column>
      <xsl:attribute name="column-width">
        <xsl:text>proportional-column-width(2)</xsl:text>
      </xsl:attribute>
    </fo:table-column>
  </xsl:template>
  
  <xsl:template mode="table.row" match="*">
    <fo:table-row xsl:use-attribute-sets="sty.row">
      <xsl:apply-templates select="@image|*" mode="table.cell" />
    </fo:table-row>
  </xsl:template>
  
  <xsl:template mode="table.cell" match="*">
    <fo:table-cell xsl:use-attribute-sets="sty.cell">
      <fo:block>
        <xsl:apply-templates />
      </fo:block>
    </fo:table-cell>
  </xsl:template>
  
  <xsl:template mode="table.cell" match="@image">
    <fo:table-cell xsl:use-attribute-sets="sty.cell">
      <fo:block>
        <xsl:element name="fo:external-graphic" use-attribute-sets="sty.graphic">
          <xsl:attribute name="src">
            <xsl:text>url('</xsl:text>
            <xsl:value-of select="." />
            <xsl:text>')</xsl:text>
          </xsl:attribute>
        </xsl:element>
      </fo:block>
    </fo:table-cell>
  </xsl:template>

</xsl:transform>

