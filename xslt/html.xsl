<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE xsl:transform [
  <!ENTITY css SYSTEM "styles.css">
]>
<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
  
  <xsl:output
    method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <xsl:strip-space elements="*"/>
  
  <xsl:variable name="CSS">&css;</xsl:variable>
  
  <xsl:template match="/*[1]">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="head" />
      <xsl:apply-templates select="." mode="body" />
    </html>
  </xsl:template>
  
  <xsl:template match="*">
    <xsl:apply-templates select="." mode="div" />
  </xsl:template>
  
  <xsl:template match="*" mode="div">
    <xsl:param name="contenu">
      <xsl:apply-templates />
    </xsl:param>
    <div class="{local-name(.)}">
      <xsl:if test="@xml:id">
        <xsl:attribute name="id">
          <xsl:value-of select="@xml:id" />
        </xsl:attribute>
      </xsl:if>
      <xsl:copy-of select="$contenu" />
    </div>
  </xsl:template>
  
  <xsl:template match="*" mode="span">
    <span class="{local-name(.)}">
      <xsl:apply-templates />
    </span>
  </xsl:template>
  
  <xsl:template match="*" mode="head">
    <head>
      <title>
        <xsl:choose>
          <xsl:when test="/profils">
            <xsl:text>catalogue de profils</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>profil de </xsl:text>
            <xsl:value-of select="normalize-space(//prénom)" />
          </xsl:otherwise>
        </xsl:choose>
      </title>
      <xsl:if test="/profil">
        <meta name="author" content="{normalize-space(//prénom)}" />
      </xsl:if>
      <meta name="keywords">
         <xsl:attribute name="content">
          <xsl:if test="/profils">
            <xsl:for-each select="profil">
              <xsl:value-of select="@xml:id" />
              <xsl:if test="position(.) != last()">
                <xsl:text>, </xsl:text>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="/profil">
            <xsl:for-each select="qualités/qualité">
              <xsl:value-of select="normalize-space(.)" />
              <xsl:if test="position() != last()">
                <xsl:text>, </xsl:text>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:attribute>
      </meta>
      <style type="text/css">
        <xsl:value-of select="normalize-space($CSS)" />
      </style>
    </head>
  </xsl:template>
  
  <xsl:template match="*" mode="body">
    <body>
      <xsl:choose>
        <xsl:when test="/profils">
          <xsl:apply-templates select="*" mode="profil" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="." mode="profil" />
        </xsl:otherwise>
      </xsl:choose>
    </body>
  </xsl:template>
  
  <xsl:template match="*" mode="profil">
    <xsl:variable name="contenu">
      <xsl:apply-templates select="@image" />
      <h1>
        <xsl:apply-templates select="prénom" />
        <xsl:apply-templates select="nom" />
      </h1>
      <h2 class="pseudo id">
        <xsl:value-of select="@xml:id" />
      </h2>
      <xsl:apply-templates select="qualités" />
      <xsl:apply-templates select="description" />
    </xsl:variable>
    <xsl:apply-templates select="." mode="div">
      <xsl:with-param name="contenu" select="$contenu" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="nom | prénom">
    <xsl:if test="preceding-sibling::*">
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:apply-templates select="." mode="span" />
  </xsl:template>

  <xsl:template match="qualité">
    <xsl:variable name="qualité.séparateur">
      <xsl:text>, </xsl:text>
    </xsl:variable>
    <xsl:apply-templates select="." mode="span" />
    <xsl:if test="following-sibling::qualité">
      <xsl:value-of select="$qualité.séparateur" />
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="@image">
      <img src="{normalize-space(.)}"
        alt="avatar" class="image avatar" />
  </xsl:template>
    
</xsl:transform>

