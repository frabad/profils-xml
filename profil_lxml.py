#!/usr/bin/env python3
import pathlib
import sys
try:
    import lxml.etree as LX
except ImportError as e:
    sys.exit(f"{e}\nfirst install the 'lxml' module")
if __name__ == "__main__":
    """
    define resources
    """
    lang = sys.argv[2] if len(sys.argv)>2 else "any"
    model = LX.DTD("profils.dtd")
    xmlip = pathlib.Path(sys.argv[1] if len(sys.argv)>1 else None)
    assert xmlip, "Path to input XML document is required."
    xmlop = xmlip.with_suffix(f".{lang}.xml")
    """
    use lxml to validate the main instance against the provided model
    """
    xmli = LX.parse(str(xmlip))
    valid = model.validate(xmli)
    if not valid: sys.exit(model.error_log)
    """
    use lxml to perform a parametric XSL-Transformation
    """
    xsli      = LX.parse("garden/xlang.xsl")
    transform = LX.XSLT(xsli)
    xmlo      = transform(xmli,lang=f"'{lang}'")
    xmlo.write(str(xmlop),encoding="utf-8")
    """
    use lxml to perform an additional XSL-Transformation
    """
    if len(transform.error_log):
        print("errors :\n",transform.error_log)
    else:
        xmli = xmlo
        xsli = LX.parse("xslt/html.xsl")
        transform = LX.XSLT(xsli)
        xmlo = transform(xmli)
        if len(transform.error_log):
            print("errors :\n",transform.error_log)
        else:
            xmlop.unlink()
            xmlop = xmlop.with_suffix(".html")
            xmlo.write(str(xmlop),encoding="utf-8")
    if xmlop.exists():
        print("%s rendered to %s" % (xmlip,xmlop))

